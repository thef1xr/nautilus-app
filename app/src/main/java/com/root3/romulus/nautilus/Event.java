package com.root3.romulus.nautilus;

/**
 * Created by romulus on 3/6/16.
 */
public class Event {
    String eventName;
    String description;
    String eventId;
    int imageId;
    // TODO: Model might need changes refer Nautilus.Heroku.app

    public Event(String eventId,String eventName, String description, int imageId){
        this.eventId = eventId;
        this.eventName = eventName;
        this.description = description;
        this.imageId = imageId;
    }
}
