package com.root3.romulus.nautilus;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by romulus on 3/6/16.
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.EventViewHolder> {
    public Context context;
    List<Event> contents;
    static final int TYPE_HEADER = 0;
    static final int TYPE_CELL = 1;
    public static class EventViewHolder extends RecyclerView.ViewHolder{
        CardView cv;
        TextView eventName;
        TextView eventDescription;
        ImageView eventImage;

        public EventViewHolder(View itemView){
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.card_view);
            eventName = (TextView)itemView.findViewById(R.id.event_name);
            eventDescription = (TextView)itemView.findViewById(R.id.event_description);
            eventImage = (ImageView)itemView.findViewById(R.id.event_image);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (getAdapterPosition() != RecyclerView.NO_POSITION) {
                        Context c = v.getContext();
                        Toast.makeText(c, "Clicks on cards finally", Toast.LENGTH_SHORT).show();
                    }
                    Log.d("Clicked card : ", "Element " + getAdapterPosition() + " clicked.");
                }
            });
        }
    }
    public RecyclerViewAdapter(Context context,List<Event> contents){
        this.context=context;
        this.contents = contents; }

    @Override
    public int getItemViewType(int position){
        switch (position) {
            case 0:
                return TYPE_HEADER;
            default:
                return TYPE_CELL;
        }
    }

    @Override
    public int getItemCount() { return contents.size(); }

    public EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view;

        switch (viewType) {
            case TYPE_HEADER: {
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_card_big, parent, false);
               return new EventViewHolder(view);
            }
            case TYPE_CELL: {
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_card_small, parent, false);
                return new EventViewHolder(view);
            }
        }
        return null;
    }

    public void onBindViewHolder(EventViewHolder holder,int position){
        holder.eventName.setText(contents.get(position).eventName);
        holder.eventDescription.setText(contents.get(position).description);
        holder.eventImage.setImageResource(contents.get(position).imageId);
    }
}
